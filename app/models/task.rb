# frozen_string_literal: true

# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  description :text             not null
#  avatar      :string           null,
#  checked     :boolean          not null, default => false
#  timestamp
#
# ======
class Task < ApplicationRecord
  # sort by updated_at
  scope :order_by_updated_at, -> { order('updated_at DESC') }
  # make sure client sends a description with the task
  validates :description, presence: true
end
