# frozen_string_literal: true

# This file contain stests for the task model.

require 'rails_helper'

RSpec.describe Task, type: :model do
  let!(:tasks) { FactoryBot.create_list(:task, 20) }

  it 'is valid with all attributes passed' do
    task = Task.new(description: 'Some description here',
                    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
                    checked: true)
    expect(task).to be_valid
  end

  it 'is not valid without a description' do
    task = Task.new(description: '',
                    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
                    checked: true)
    expect(task).to_not be_valid
  end

  it 'returns tasks by according to most recently updated' do
    random_task = tasks.sample
    random_task.update(updated_at: Time.now)
    recent_tasks = Task.order_by_updated_at
    expect(random_task.id).to eq(recent_tasks.first.id)
  end
end
