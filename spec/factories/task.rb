# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    description { 'I would have used Faker gem but this is a small api' }
    avatar { 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg' }
    checked { [true, false].sample }
  end
end
