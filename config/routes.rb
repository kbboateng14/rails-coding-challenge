# frozen_string_literal: true

Rails.application.routes.draw do
  # scope routes to allow for versioning
  namespace :api do
    namespace :v1 do
      resources :tasks, only: %i[index create update]
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
