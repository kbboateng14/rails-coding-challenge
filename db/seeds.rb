# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

tasks = [
  {
    description: 'Romantic Spots for Honeymoons',
    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
    checked: false
  },
  {
    description: 'Top Island Getaways',
    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
    checked: false
  },
  {
    description: 'Most Romantic Destinations',
    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
    checked: false
  },
  {
    description: 'Top Spots for Skiing',
    avatar: 'https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg',
    checked: true
  }
]

tasks.each do |task|
  Task.create(description: task[:description],
              avatar: task[:avatar],
              checked: task[:checked])
end
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
