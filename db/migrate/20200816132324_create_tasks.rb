# frozen_string_literal: true

class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.text :description, null: false
      t.string :avatar
      t.boolean :checked, default: false

      t.timestamps
    end
  end
end
