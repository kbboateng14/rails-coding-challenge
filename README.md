# Welcome to DoubleGDP Ruby on Rails Coding Challenge

## What is this project?

This a web based application programming interface(API) that provides the
client with everything to create, update and view a task.

## Getting Started

1.  Install ruby 2.6 or higher with the command prompt if you haven't yet.

2.  Install Bundler at the command prompt if you haven't yet, this will be used to manage the application dependencies:

        $ gem install bundler

3.  At the command prompt, change directory to `task_api` after cloning this repository where "task_api" is the name of the project:

        $ cd task_api/

4.  At the command prompt, run bundle install in the application root directory to install all dependencies:

        $ bundle install

5.  At the command prompt, create test and development databases locally:

        $ rails db:create

6.  At the command prompt, run database migrations to set up your schema:

        $ rails db:migrate

7.  At the command prompt, seed the database to have data to work with:

        $ rails db:seed

8.  At the command prompt, run tests to ensure everything is in shape:

        $ bundle exec rspec .

9.  At the command prompt, run the rails server on port 4000 to start the API:

        $ rails server -p 4000

    Make sure your rails server listens at port 4000 since the client will be fetching from port 4000

    Run with `--help` or `-h` for options.

10. Follow the guidelines to learn more about rails. You may find
    the following resources handy:
    - [Getting Started with Rails](https://guides.rubyonrails.org/getting_started.html)
    - [Ruby on Rails Guides](https://guides.rubyonrails.org)
    - [The API Documentation](https://api.rubyonrails.org)
